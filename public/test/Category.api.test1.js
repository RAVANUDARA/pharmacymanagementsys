/**
 * Created by YASITHA on 7/2/2017.
 */
var should = require('should');
var request = require('request');
var expect = require('chai').expect;
var util =require('util');
var mocha=require('mocha');
var supertest = require('supertest');

var baseUrl = 'http://localhost:3000';




var apiPath='/CategoryDetails';

describe(apiPath+"_GET" , function () {
    it("GET", function (done) {
        request.get({url: baseUrl + apiPath},
            function (err, res, body) {
                if (err) {
                    console.log('')
                }
                expect(res.statusCode).to.equal(200);
                console.log('\nGET :Test 1 for ' + apiPath + ' is pass');
                done();

            })
    })

})


describe(apiPath+"_POST", function () {
    it("POST", function (done) {
        request.post({url: baseUrl + apiPath},
            function (err, res, body) {
                if (err) {
                    console.log('')
                }
                expect(res.statusCode).to.equal(200);
                console.log('\n Post :Test 1 for ' + apiPath + ' is pass');
                done();

            })
    })

})


describe(apiPath+"_PUT", function () {
    it("PUT", function (done) {
        request.put({url: baseUrl + apiPath},
            function (err, res, body) {
                if (err) {
                    console.log('')
                }
                expect(res.statusCode).to.equal(200);
                console.log('\n put :Test 1 for ' + apiPath + ' is pass');
                done();

            })
    })

})




describe(apiPath+"_DELETE", function () {
    it("DELETE", function (done) {
        request.delete({url: baseUrl + apiPath},
            function (err, res, body) {
                if (err) {
                    console.log('')
                }
                expect(res.statusCode).to.equal(200)

                console.log('\n Delete :Test 1 for ' + apiPath + ' is pass');
                done();

            })
    })
})