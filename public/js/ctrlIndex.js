angular.module('App')
    .controller('ctrlIndex', function ($rootScope, $scope, $http, $location) {
        $rootScope.user ={};
        $scope.logout = function () {
            localStorage.clear();
            $location.path('');
            location.reload();
        }
    })
