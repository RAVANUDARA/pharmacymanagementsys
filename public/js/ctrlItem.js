angular.module('App')
.controller('ctrlItem', function ($scope, $http) {
    $scope.search;
    $scope.items = [];
    $scope.item= {};

    $scope.isMinusReOrderLevel=function(val)
    {
        if($scope.item.ReOrderLevel <0)
        {
            $scope.item.ReOrderLevel =0;
            window.alert('You are not allowed to enter minus values')
        }
    }

	//this is isMinusSellPrice sale func
    $scope.isMinusSellPrice=function(val)
    {
        if($scope.item.SellPrice <0)
        {
            $scope.item.SellPrice =0;
            window.alert('You are not allowed to enter minus values')
        }
    }

	//this is getItems sale func
    $scope.getItems=function() {
        $http.get('/ItemMasterDetails')
            .then(function (result) {
                $scope.items = result.data;
            })
            .catch(function (data, status) {
                log.consoles(status);
            })
    }

    $scope.getItems();


    // this func use for add value for controllers
    $scope.addValueForControllers = function (id) {
        $scope.item = $scope.items.find(function (curVal) {
            return id === curVal._id;
        })
        console.log($scope.item)
    }

    // this is for add new values
    $scope.addItem =function (){
        $http.post('/ItemMasterDetails',$scope.item)
            .then(function (result) {
                // $scope.items= result.data;
                $scope.getItems();
            })
            .catch(function (data, status) {

            })
        clearControllers();
    }

    //this function for delete values
    $scope.deleteItem = function (id) {
        console.log(id)
        $http.delete('/ItemMasterDetails/' + id, {})
            .then(function (result) {
                // $scope.items = result.data;
                $scope.getItems();
            })
            .catch(function (data, status) {
            })
    }

    //Search Item
    $scope.serchItem =function (findname) {
        $http.get('/ItemMasterDetails/' + findname)
            .then(function (result) {
                $scope.items = result.data;
            })
            .catch(function (data, status) {
                log.consoles(status);
            })
    }
    //Search Item by category
    $scope.serchItemByCategory =function (findname) {
        $http.get('/ItemMasterDetailsBycatogory/' + findname)
            .then(function (result) {
                $scope.items = result.data;
            })
            .catch(function (data, status) {
                log.consoles(status);
            })
    }

    //clear all controllers
    $scope.clearControllers = function (){
        $scope.item = null;
    }


    // this function for Update Item
    $scope.updateitem=function () {
        $http.put('/ItemMasterDetails/',$scope.item)
            .then(function (result) {
                $scope.items= result.data;
                window.alert('The Item Is Updated')
                // $scope.clearControllers()
                $scope.getItems();
            })
            .catch(function (data, status) {

            })
    }

    $scope.generateRPT = function(){

        var ItemList;
        var columns = [
            {title: "Code", dataKey: "_id"},
            {title: "Item Name", dataKey: "ItemName"},
            {title: "Category", dataKey: "product_id"},
            {title: "Details", dataKey: "Description"},
            {title: "Type", dataKey: "Type"},
            {title: "Re Order Level", dataKey: "ReOrderLevel"},
            {title: "Sell Price", dataKey: "SellPrice"}

        ];

        var currentTime = new Date()
        var month = currentTime.getMonth() + 1
        var day = currentTime.getDate()
        var year = currentTime.getFullYear()
        var date = year + '-' + month + '-' +day;

        var doc = new jsPDF('p', 'pt');
        doc.autoTable(columns, $scope.items, {
            styles: {fillColor: [172, 255, 253]},
            columnStyles: {
                id: {fillColor: 255}
            },
            margin: {top: 60},
            addPageContent: function(data) {
                doc.text("Drug Item List", 40, 30),
                    doc.text("Printed Date : " +  date, 40, 50)
            }
        });

        date = year + '_' + month + '_' +day;
        doc.save('ItemMasterList_' + date + '.pdf');
    }
})