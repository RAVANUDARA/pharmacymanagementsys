const mongoose = require('./dbConector')

const SupplierSchema = mongoose.Schema({
    "name"  : String,
    "add"   : String,
    "mob"   : Number,
    "land"  : Number,
    "fax"  : Number,
    "catogary" : String,
    "regNo" : String,
    "editUser" : String,
    "editDate" : {type:Date, default: Date.now}
})

const Supplier = mongoose.model('Suppliers',SupplierSchema);
module.exports = Supplier;
