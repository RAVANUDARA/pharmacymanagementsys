/**
 * Created by YASITHA on 7/1/2017.
 */

'use strict'

const mongoose = require('./dbConector')

// var url='mongodb://localhost:27017/pharmacySystem';
// mongoose.connect(url);
// var db = mongoose.connection;

const CategorySchema = mongoose.Schema({
    "_id"           : String,
    "CategoryName"      : String,
    "Description"   :  String,
    "categoryType"          :  String
})

const category = mongoose.model('Category',CategorySchema);
module.exports =category;