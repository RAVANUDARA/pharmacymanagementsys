/**
 * Created by TharinduGunarathna on 5/11/2017.
 */
'use strict'
const mongoose = require('./dbConector')

const prescriptionSchema = mongoose.Schema(
    {
        patientId : {
                type:String,
                required:true
        },
        prescriptionId:{
                type:String,
                required:true
        },
        patientName : {
                type:String,
                required:true
        },
        patientAge:{
                type:Number,
                required:true
        },
        createdDate:{
                type:Date,
                required:true
        },
        prescriptionDate:{
                type:Date,
                required:true
        },
        drugs:[{
                drugId:String,
                name:String,
                dosage:Number,
                frequency:Number,
                period:Number,
                quantity:Number
        }]
    },{collection:'prescription'});

const prescription = mongoose.model('prescription',prescriptionSchema);
module.exports = prescription;