angular.module('App')
.controller('ctrlCategory', function ($scope, $http) {

    $scope.categories=[];
    $scope.category=[];
    $scope.search;

    $scope.pritadded=function(){
        console.log($scope.category)
        $http.post('/CategoryDetails',$scope.category)
    }

    //add new category
    $scope.addCategory = function()  {
        $http.post('/CategoryDetails',{
            _id:$scope.category._id,
            CategoryName     :$scope.category.CategoryName,
            Description   :  $scope.category.Description,
            categoryType:$scope.category.categoryType})

            .then(function (result) {
                $scope.getCatogory();
                console.log("Getting categories")
            })
            .catch(function (data, status) {

            })
    }

    //get all categories
    $scope.getCatogory= function() {
        $http.get('/CategoryDetails')
            .then(function (result) {
                $scope.categories = result.data;
            })
            .catch(function (data, status) {
            })

        // $scope.clearControllers();
    }

    $scope.getCatogory();

    //search a new category
    $scope.serchcat=function (findname) {
        $http.get('/CategoryDetails/' + findname)
            .then(function (result) {
                $scope.categories = result.data;
            })
            .catch(function (data, status) {

            })
    }

    //select category
    $scope.addValueForControllers = function (id) {
        $scope.category = $scope.categories.find(function (curVal) {
            return id === curVal._id;
        })
    }

    //Delete Category by Id
    $scope.deleteProduct= function (id) {
        $http.delete('/CategoryDetails/' + id, {})
            .then(function (result) {
                //$scope.categories = result.data;
                window.alert("Deleted Success fully")
                $scope.getCatogory();
            })
            .catch(function (data, status) {
            })
    }

    // this function for Update Categories
    $scope.updateCategory=function () {
        $http.put('/CategoryDetails',{
            _id:$scope.category._id,
            CategoryName     :$scope.category.CategoryName,
            Description   :  $scope.category.Description,
            categoryType:$scope.category.categoryType})
            .then(function (result) {
                // $scope.categories= result.data;
                window.alert('The Item Is Updated')
                $scope.getCatogory();
                $scope.clearControllers();

            })
            .catch(function (data, status) {
                log.consoles(data);
            })
    }


    //clear all controllers
    $scope.clearControllers = function (){
        $scope.category = null;
    }
})/**
 * Created by Sachintha on 7/2/2017.
 */
