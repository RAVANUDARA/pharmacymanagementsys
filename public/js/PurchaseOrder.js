/**
 * Created by YASITHA on 7/1/2017.
 */


'use strict'

const mongoose = require('./dbConector')

const PurchaseOrdrSchema = mongoose.Schema({
    "_id"               : String,
    "PDate"             :String,
    "items" : [],
    "received" : Boolean,
    "user"              :String
})
const purchasOrder = mongoose.model('purchaseOrder',PurchaseOrdrSchema);
module.exports =purchasOrder;