const mongoose = require('./dbConector')

const UserSchema = mongoose.Schema({
    "name"  : String,
    "pass"   : String,
    "type" : String,
    "editUser" : String,
    "editDate" : {type:Date, default: Date.now}
})

const User = mongoose.model('user',UserSchema);
module.exports = User;
