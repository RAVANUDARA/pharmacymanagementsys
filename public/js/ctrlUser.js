angular.module('App')
    .controller('ctrlUser', function ($scope, $http) {
        $scope.users = [];
        $scope.user = {};
        $scope.selected = false;

        //this func use for insert new data for user master table
        $scope.submitClick = function(){
            //$scope.user.editUser = $rootScope.user.name
            $scope.user.editUser = "test"
            $http.post('/users', $scope.user ).then(function (res) {
                $scope.users.push(res.data);
                console.log(res.data)
                $scope.clearControllers();
            });
        }

        // this func use for add value for controllers
        $scope.addValueForControllers = function (id) {
            $scope.selected = true;
            $scope.user = $scope.users.find(function(curVal){
                return id === curVal._id;
            });
        }

        $scope.updateClick = function () {
            $scope.user.editUser = "test"
            $http.put(('/users/'+$scope.user._id), $scope.user).then(function (res) {
                $scope.clearControllers();
            });
        }

        $scope.deleteClick = function () {
            $http.delete(('/users/'+$scope.user._id)).then(function (res) {
                getSupplier();
                $scope.clearControllers();
            });
        }

        // get user master data
        function getSupplier() {
            $http.get('/users').then(function (res) {
                $scope.users = res.data;
            })
        }

        getSupplier();

        //clear all controllers
        $scope.clearControllers = function (){
            $scope.selected = false;
            $scope.user = null;
        }

        $scope.generateRPT = function(){

            // get prescription master data
            var prescriptionList;
            var columns = [
                {title: "Name", dataKey: "name"},
                {title: "Type", dataKey: "add"},
                {title: "Edit User", dataKey: "editUser"},
                {title: "Edit Date", dataKey: "editDate"}
                // {title: "Reg No", dataKey: "regNo"},
                // {title: "Edit User", dataKey: "editUser"},
                // {title: "Edit Date", dataKey: "editDate"}
            ];

            var currentTime = new Date()
            var month = currentTime.getMonth() + 1
            var day = currentTime.getDate()
            var year = currentTime.getFullYear()
            var date = year + '-' + month + '-' +day;

            var doc = new jsPDF('p', 'pt');
            doc.autoTable(columns, $scope.users, {
                styles: {fillColor: [172, 255, 253]},
                columnStyles: {
                    id: {fillColor: 255}
                },
                margin: {top: 60},
                addPageContent: function(data) {
                    doc.text("User List", 40, 30),
                        doc.text("Printed Date : " +  date, 40, 50)
                }
            });

            date = year + '_' + month + '_' +day;
            doc.save('User_' + date + '.pdf');
        }
    })
