angular.module('App')
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $stateProvider
            .state('supplierMaster', {
                templateUrl : "public/template/SupplierMaster.html",
                controller : "ctrlSupplier"
            })
            .state('userMaster', {
                templateUrl : "public/template/UserMaster.html",
                controller : "ctrlUser"
            })
            .state('itemMaster', {
                templateUrl : "public/template/Item.html",
                controller : "ctrlItem"
            })
            .state('categorytMaster', {
                templateUrl : "public/template/Category.html",
                controller : "ctrlCategory"
            })
            .state('purchaseOrder', {
                templateUrl : "public/template/PurchaseOrder.html",
                controller : "ctrlPurchaseOrder"
            })
            .state('grnAndBatch', {
                templateUrl : "public/template/BatchGRN.html",
                controller : "ctrlBatch"
            })
            .state('prescription', {
                templateUrl : "public/template/Prescription.html",
                controller : "ctrlPrescription"
            })
            .state('dispense', {
                templateUrl : "public/template/Dispense.html",
                controller : "ctrlDispense"
            })
            .state('viewDispense', {
                templateUrl : "public/template/ViewDispense.html",
                controller : "ctrlDispense"
            })

        //$urlRouterProvider.otherwise('/SupplierMaster');
        $locationProvider.html5Mode(true).hashPrefix('');
    })
