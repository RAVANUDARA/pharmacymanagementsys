angular.module('App')
.controller("ctrlPrescription",function($scope, $http){

    $scope.selectedPrescription = {};
    $scope.newPrescription = {};
    $scope.selectedDrug = {};
    $scope.selectedPrescribedDrug = {};
    $scope.updatedPrescription = {};
    $scope.selectedPrescribedDrugs = [];
    $scope.searchedPatientId = '';

    //flags
    $scope.viewFlag = false;
    $scope.saveFlag = false;
    $scope.updateFlag = false;
    $scope.searchFlag = false;
    $scope.reportFlag = false;

    //get all prescriptions from DB
    $scope.getPrescriptions  = function(){
        $http.get('/prescriptions').then(function (response) {
            $scope.prescriptions = response.data;
        });
    }

    $scope.getPrescriptions();

    //view the selected prescription in the modal
    $scope.viewPrescription = function (index) {
        $scope.selectedPrescription = $scope.prescriptions[index];

        $scope.viewFlag = true;
        $scope.saveFlag = false;
        $scope.updateFlag = false;
    }

    //pick a drug from the table fed by DB
    $scope.pickDrugs = function (index) {
        $scope.selectedDrug = $scope.drugs[index];
    }

    $scope.getCurrentDate = function () {
        var currentDate = new Date();
        return currentDate.getFullYear()+"-"+(currentDate.getMonth()+1)+"-"+currentDate.getDate();
    }

    $scope.addPrescription = function () {
        $scope.viewFlag = false;
        $scope.saveFlag = true;
        $scope.updateFlag = false;
    }

    $scope.insertPrescription = function () {
        $scope.newPrescription.prescriptionDate = $scope.getCurrentDate();
        $scope.newPrescription.drugs = $scope.selectedPrescribedDrugs;
        $http.post('/prescriptions',$scope.newPrescription).then(function () {
            $scope.getPrescriptions();
            alert('New prescription was added successfully');
        });
    }

    $scope.upadatePrescription = function () {
        $scope.selectedPrescription.prescriptionId = $scope.updatedPrescription.prescriptionId;
        $scope.selectedPrescription.patientId = $scope.updatedPrescription.patientId;
        $scope.selectedPrescription.patientName = $scope.updatedPrescription.patientName;
        $scope.selectedPrescription.patientAge = $scope.updatedPrescription.patientAge;

        for(i=0;i<$scope.selectedPrescribedDrugs.length;i++)
        {
            $scope.selectedPrescription.drugs.push($scope.selectedPrescribedDrugs[i]);
        }

        $http.put('/prescriptions/'+$scope.selectedPrescription._id,$scope.selectedPrescription).then(function () {
            $scope.getPrescriptions();
            alert('Prescription was updated successfully');
        });

    }

    $scope.deletePrescription = function () {
        $http.delete('/prescriptions/'+$scope.selectedPrescription._id).then(function () {
            $scope.getPrescriptions();
            alert('Prescription was deleted successfully');
        });
    }

    $scope.getDrugs = function(){
        $http.get('/drugs').then(function (response) {
            $scope.drugs = response.data;
        });
    }

    $scope.addToSelectedDrugs = function () {
        $scope.selectedPrescribedDrug.drugId = $scope.selectedDrug.product_id;
        $scope.selectedPrescribedDrug.name = $scope.selectedDrug.ItemName;
        $scope.selectedPrescribedDrug.quantity =
            ($scope.selectedPrescribedDrug.dosage*$scope.selectedPrescribedDrug.frequency*$scope.selectedPrescribedDrug.period);
        $scope.selectedPrescribedDrugs.push($scope.selectedPrescribedDrug);
        $scope.selectedPrescribedDrug = {};
    }

    $scope.enableUpdateButton = function () {
        $scope.updatedPrescription.prescriptionId = $scope.selectedPrescription.prescriptionId;
        $scope.updatedPrescription.patientId = $scope.selectedPrescription.patientId;
        $scope.updatedPrescription.patientName = $scope.selectedPrescription.patientName;
        $scope.updatedPrescription.patientAge = $scope.selectedPrescription.patientAge;

        $scope.viewFlag = false;
        $scope.saveFlag = false;
        $scope.updateFlag = true;
    }

    $scope.searchPrescriptionByPatientId = function () {
        $http.get('/patients/'+$scope.searchedPatientId).then(function (response) {
            $scope.prescriptions = response.data;
        });
        $scope.searchFlag = true;
    }

    $scope.getAllPrescriptions = function () {
        $scope.getPrescriptions();
        $scope.searchFlag = false;
        $scope.searchedPatientId = '';
    }

    $scope.generateRPT = function(){

        // get prescription master data
        var prescriptionList;
        var columns = [
            {title: "PRESCRIPTION ID", dataKey: "prescriptionId"},
            {title: "PATIENT ID", dataKey: "patientId"},
            {title: "PATIENT NAME", dataKey: "patientName"},
            {title: "PATIENT AGE", dataKey: "patientAge"},
            {title: "CREATED DATE", dataKey: "createdDate"},
            {title: "PRESCRIPTION DATE", dataKey: "prescriptionDate"}
            // {title: "Reg No", dataKey: "regNo"},
            // {title: "Edit User", dataKey: "editUser"},
            // {title: "Edit Date", dataKey: "editDate"}
        ];

        var currentTime = new Date()
        var month = currentTime.getMonth() + 1
        var day = currentTime.getDate()
        var year = currentTime.getFullYear()
        var date = year + '-' + month + '-' +day;

        var doc = new jsPDF('p', 'pt');
        doc.autoTable(columns, $scope.prescriptions, {
            styles: {fillColor: [172, 255, 253]},
            columnStyles: {
                id: {fillColor: 255}
            },
            margin: {top: 60},
            addPageContent: function(data) {
                doc.text("Prescription List", 40, 30),
                    doc.text("Printed Date : " +  date, 40, 50)
            }
        });

        date = year + '_' + month + '_' +day;
        doc.save('PrescriptionList_' + date + '.pdf');
    }

    // $scope.generateReport = function () {
    //     $http.post('/reports',$scope.selectedPrescription).then(function () {
    //         alert('Prescription report will be downloaded');
    //     });
    // }

})

// module.config(function ($routeProvider) {
//     $routeProvider
//         .when("/prescription",{
//         templateUrl:"index.html"
//     });
// });





