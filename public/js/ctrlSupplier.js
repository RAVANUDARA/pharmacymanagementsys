angular.module('App')
.controller('ctrlSupplier', function ($rootScope, $scope, $http) {
    $scope.suppliers = [];
    $scope.supplier = {};
    $scope.selected = false;

    //this func use for insert new data for supplier master table
    $scope.submitClick = function(){
        //$scope.supplier.editUser = $rootScope.user.name
        $scope.supplier.editUser = "test"
        $http.post('/suppliers', $scope.supplier ).then(function (res) {
            $scope.suppliers.push(res.data);
            console.log(res.data)
            $scope.clearControllers();
        });
    }

    // this func use for add value for controllers
    $scope.addValueForControllers = function (id) {
        $scope.selected = true;
        $scope.supplier = $scope.suppliers.find(function(curVal){
            return id === curVal._id;
        });
    }

	//this func using for update supplier
    $scope.updateClick = function () {
        $scope.supplier.editUser = "test"
        $http.put(('/suppliers/'+$scope.supplier._id), $scope.supplier).then(function (res) {
            $scope.clearControllers();
        });
    }

	//this func using for delete supplier
    $scope.deleteClick = function () {
        $http.delete(('/suppliers/'+$scope.supplier._id)).then(function (res) {
            getSupplier();
            $scope.clearControllers();
        });
    }

    // get supplier master data
    function getSupplier() {
        $http.get('/suppliers').then(function (res) {
            $scope.suppliers = res.data;
        })
    }

    $http.get('/CategoryDetails').then(function (res) {
        $scope.catogarys = res.data;
    })

    getSupplier();

    //clear all controllers
    $scope.clearControllers = function (){
        $scope.selected = false;
        $scope.supplier = null;
    }

    $scope.generateRPT = function(){

        // get supplier master data
        var supList;
        var columns = [
            {title: "NAME", dataKey: "name"},
            {title: "ADDRESS", dataKey: "add"},
            {title: "MOBILE", dataKey: "mob"},
            {title: "LAND", dataKey: "land"},
            {title: "FAX", dataKey: "fax"},
            {title: "CATEGORY", dataKey: "catogary"},
            {title: "Reg No", dataKey: "regNo"},
            {title: "Edit User", dataKey: "editUser"},
            {title: "Edit Date", dataKey: "editDate"}
        ];

        var currentTime = new Date()
        var month = currentTime.getMonth() + 1
        var day = currentTime.getDate()
        var year = currentTime.getFullYear()
        var date = year + '-' + month + '-' +day;

        var doc = new jsPDF('p', 'pt');
        doc.autoTable(columns, $scope.suppliers, {
            styles: {fillColor: [172, 255, 253]},
            columnStyles: {
                id: {fillColor: 255}
            },
            margin: {top: 60},
            addPageContent: function(data) {
                doc.text("Supplier List", 40, 30),
                doc.text("Printed Date : " +  date, 40, 50)
            }
        });

        date = year + '_' + month + '_' +day;
        doc.save('SupplierList_' + date + '.pdf');
    }
})