angular.module('App')
.controller('ctrlLogin', function ($rootScope, $scope, $http, $location) {
    $rootScope.user ={};
    $scope.checkValidUser = function () {
        $http.post('/login', $rootScope.user).then(function (res) {
            var User = res.data;
            if(User.length > 0){
                $rootScope.user = User[0]
                console.log($rootScope.user)
                localStorage.clear();
                $location.path('index');
                location.reload();
            }else
                $scope.errorMsg = "Please enter correct user name & password";
        })
    }
})