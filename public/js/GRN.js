/**
 * Created by YASITHA on 7/1/2017.
 */

'use strict'

const mongoose = require('./dbConector')

const GRNSchema = mongoose.Schema({
    "GRN_CODE"               : String,
    "PDate"             :{type:Date, default: Date.now},
    "items" : [],
    "user"              :String
})
const GRNOrder = mongoose.model('GRN',GRNSchema);
module.exports =GRNOrder;