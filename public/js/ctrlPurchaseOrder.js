angular.module('App')
    .controller('ctrlPurchaseOrder',function ($scope, $http) {

        $scope.itemList=[];
        $scope.selectitem={};
        $scope.poList=[];
        $scope.po=[];

        var temArr=[];

        //To set Selected Item list
        $scope.addToList = function () {
            temArr.push($scope.selectitem);
            $scope.itemList =temArr;
            console.log($scope.itemList)
            $scope.selectitem = null;
        }


        $scope.deleteItem = function (val) {

            for(var i = 0; i < temArr.length; i++) {

                if(temArr.ItemName === val) {
                    temArr.splice(i, 1);
                }
            }
            temArr.push($scope.selectitem);
            $scope.itemList =temArr;
            console.log($scope.itemList)
            $scope.selectitem = null;
        }



        $scope.isMinusQty=function(val)
        {
            if($scope.selectitem.qty <0)
            {
                $scope.selectitem.qty =0;
                window.alert('You are not allowed to enter minus values')
            }
        }

        //add new purchase order
        $scope.addPruchaseOrder = function()  {
            console.log($scope.po)
            $http.post('/puchaseOrder',{
                _id         : $scope.po._id,
                PDate             :$scope.po.pDate+"",
                IsCancelled       :"false",
                Isapproved      :"false",
                items : temArr,
                received : false,
                User              :"Admin"
            })
                .then(function (result) {
                    // $scope.poList= result.data;
                    $scope.getPurchaseOrder();
                    console.log($scope.po.pDate)
                })
                .catch(function (data, status) {
                    log.consoles(data);
                })

        }



        //get all categories
       $scope.getPurchaseOrder= function() {
           $http.get('/puchaseOrder')
               .then(function (result) {
                   $scope.poList = result.data;

                   $scope.category=null;
               })
               .catch(function (data, status) {
                   log.consoles(status);
               })
       }

        $scope.getPurchaseOrder();

        //Delete Category by Id
        $scope.deletePO= function (id) {
            $http.delete('/puchaseOrder/' + id, {})
                .then(function (result) {
                    //$scope.poList = result.data;
                    $scope.getPurchaseOrder();
                    window.alert("Deleted Success fully")
                })
                .catch(function (data, status) {
                })
        }

    })
