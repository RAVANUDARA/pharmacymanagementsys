const mongoose = require('./dbConector')

const StockBalanceSchema = mongoose.Schema({
    "grnID"  : String,
    "batchId"   : String,
    "ItemName" : String,
    "qty" : Number,
    "editUser" : String,
    "editDate" : {type:Date, default: Date.now}
})

const StockBalace = mongoose.model('StockBalance',StockBalanceSchema);
module.exports = StockBalace;
