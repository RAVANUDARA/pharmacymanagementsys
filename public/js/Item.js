/**
 * Created by YASITHA on 5/3/2017.
 */
'use strict'
const mongoose = require('./dbConector')

const ItemSchema = mongoose.Schema({
    "_id"           : String,
    "product_id"	: String,
    "ItemName"      : String,
    "Description"   :  String,
    "Type"          :  String,
    "ReOrderLevel"  :  Number,
    "SellPrice"     :  Number,
    "User"          : String
})

const Item = mongoose.model('Items',ItemSchema);
module.exports =Item;