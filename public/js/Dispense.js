'use strict'

const mongoose=require('mongoose');

const Schema = mongoose.Schema;

const dispenseSchema = new Schema(
    {
        paymentId: String,
        patientId:String,
        prescriptionId:String,
        drugs:[{
            drugId:String,
            name:String,
            quantity:Number
        }],


        unitPrice:Number,
        paymentPerDrug:Number,
        totalPayment:Number

    });



const dispense= mongoose.model('Dispense',dispenseSchema);
module.exports = dispense;