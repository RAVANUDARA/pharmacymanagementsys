'use strict'

const  mongoose = require('mongoose');
//------- bellow code only work for EC6 and it must need avoid mongoose promise errors --------
mongoose.Promise = global.Promise;
//------------------------------------- end ---------------------------------------------------
var url='mongodb://localhost:27017/pharmacySystem';
mongoose.connect(url);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error'));

module.exports =  mongoose;

