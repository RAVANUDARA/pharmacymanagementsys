angular.module('App')
.controller('ctrlDispense', ['$scope','$http','$location', function($scope, $http, $location){


    $http.get('/dispense').then(function (res) {
        $scope.Dispenses = res.data
    })

    $scope.addDispense = function(){
        const dispense = {
            paymentId : $scope.payemntId,
            patientId : $scope.patientId,
            prescriptionId : $scope.prescriptionId,
            drugId : $scope.drugId,
            drugName : $scope.drugName,
            quantity : $scope.quantity,
            unitPrice: $scope.unitPrice,
            paymentPerDrug : $scope.paymentPerDrug,
            totalPayment: $scope.totalPayment,


        };
        $http({
            method:'POST',
            url:'/dispense',
            data: dispense
        }).then(function (response) {
            console.log(response.data);
            $scope.clear();
        },function (error) {
            console.log(error);
        });
        // $location.path('/user/viewDispense');
    };

    $scope.clear = function(){
        $scope.paymentId ="";
        $scope.patientId="";
        $scope.prescriptionId="";
        $scope.drugId="";
        $scope.drugName="";
        $scope.quantity="";
        $scope.unitPrice="";
        $scope.paymentPerDrug="";
        $scope.totalPayment="";

    };

    $scope.reset = function() {
        $http({
            method:"PUT",
            url:'/dispense',
            data:$scope.dispenseEdit
        }).then(function(success){

        },function(error){
            console.error(error);
        })
    };

    $scope.deleteDispense = function () {
        const Url = '/dispense/'+$scope.deletepayementId;

        $http({
            method:"DELETE",
            url:Url
        }).then(function(success){
            console.log(success.data);
            $scope.loadQuestionnaires();
        },function(error){
            console.error(error);
        });
    };



    $scope.deleteModel = function (id) {
        $scope.deletepaymentId = id;
    };

    $scope.setEditDespense= function (dispense) {
        $scope.dispenseEdit = dispense;
    };

    $scope.generateRPT = function(){

        // get supplier master data
        var supList;
        var columns = [
            {title: "Id", dataKey: "paymentId"},
            {title: "patientId", dataKey: "patientId"},
            {title: "prescriptionId", dataKey: "prescriptionId"},
            {title: "Unit Price", dataKey: "unitPrice"},
            {title: "FAX", dataKey: "fax"},
            {title: "CATEGORY", dataKey: "catogary"},
            {title: "Payment Per Drug", dataKey: "paymentPerDrug"},
            {title: "Total Payment", dataKey: "totalPayment"}
        ];

        var currentTime = new Date()
        var month = currentTime.getMonth() + 1
        var day = currentTime.getDate()
        var year = currentTime.getFullYear()
        var date = year + '-' + month + '-' +day;

        var doc = new jsPDF('p', 'pt');
        doc.autoTable(columns, $scope.Dispenses, {
            styles: {fillColor: [172, 255, 253]},
            columnStyles: {
                id: {fillColor: 255}
            },
            margin: {top: 60},
            addPageContent: function(data) {
                doc.text("Dispense Detail", 40, 30),
                    doc.text("Printed Date : " +  date, 40, 50)
            }
        });

        date = year + '_' + month + '_' +day;
        doc.save('Dispense_' + date + '.pdf');
    }
}]);