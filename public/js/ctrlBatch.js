angular.module('App')
    .controller('ctrlBatch', function ($rootScope, $scope, $http) {
        $scope.GRN = {};
        $scope.purchaseOrder = {};
        $scope.selected = false;
        //$scope.items = [];
        $scope.grnSelect = false
        //this func use for insert new data for supplier master table
        $scope.submitClick = function(){
            //$scope.supplier.editUser = $rootScope.user.name
            var grn = {
                GRN_CODE : $scope.purchaseOrder._id,
                PDate : new Date(),
                items : $scope.purchaseOrder.items,
                user :'test'
            }
            $http.post('/GRN', grn).then(function (res) {
                console.log(res.data)
            });

            var tempItem = $scope.purchaseOrder.items;

            for(var i = 0; i<tempItem.length; i++){
                var StockBalance = {
                    grnID : grn.GRN_CODE,
                    ItemName : tempItem[i].ItemName,
                    qty : items[i].qty,
                    editUser : 'test'
                }
                $http.post('/stockBalance', StockBalance).then(function (res) {
                    console.log(res.data)
                });
            }
            $scope.clearControllers();
        }

        $scope.updateItem = function () {
            while ($scope.purchaseOrder.items.indexOf($scope.item) !== -1) {
                $scope.purchaseOrder.items.splice($scope.purchaseOrder.items.indexOf($scope.item), 1);
            }
            $scope.purchaseOrder.items.push($scope.item);
        }

        // this func use for add value for controllers
        $scope.addValueForControllers = function (id) {
            //$scope.selected = true;
            $scope.item = $scope.purchaseOrder.items.find(function(curVal){
                return id === curVal.ItemName;
            });
        }

        $scope.addValueForControllersGRN = function (id) {
            //$scope.selected = true;
            $scope.item = $scope.GRN.items.find(function(curVal){
                return id === curVal.ItemName;
            });
        }

        $scope.select = function () {
            $scope.selected = true;
        }

        $scope.updateClick = function () {
            $scope.GRN.editUser = "test"
            $http.put(('/GRN/'+$scope.GRN._id), $scope.GRN).then(function (res) {
                $scope.clearControllers();
            });
        }

        $scope.deleteClick = function () {
            $http.delete(('/GRN/'+$scope.GRN._id)).then(function (res) {
                $scope.clearControllers();
            });
        }

        // get supplier master data
        $scope.getGRN = function(code) {
            $scope.grnSelect = true
            $http.get('/GRN/'+code).then(function (res) {
                $scope.GRN = res.data[0];
            })
        }

        $scope.getPurchaseOrder = function (purchNo) {
            $scope.grnSelect = false
            $http.get('/puchaseOrder/'+purchNo).then(function (res) {
                $scope.purchaseOrder = res.data;
            })
        }

        //clear all controllers
        $scope.clearControllers = function (){
            $scope.selected = false;
            $scope.code = null;
            $scope.purchNo = null;
            $scope.item = null;
            $scope.GRN = null;
            $scope.purchaseOrder = null;
        }

        $scope.generateRPT = function(){
            var columns = [
                {title: "NAME", dataKey: "ItemName"},
                {title: "Qnty", dataKey: "qty"}
            ];

            var currentTime = new Date()
            var month = currentTime.getMonth() + 1
            var day = currentTime.getDate()
            var year = currentTime.getFullYear()
            var date = year + '-' + month + '-' +day;

            var doc = new jsPDF('p', 'pt');
            doc.autoTable(columns, $scope.GRN.items, {
                styles: {fillColor: [172, 255, 253]},
                columnStyles: {
                    id: {fillColor: 255}
                },
                margin: {top: 60},
                addPageContent: function(data) {
                    doc.text("Good Receive Note", 40, 30),
                        doc.text("Printed Date : " +  date, 40, 50)
                }
            });

            date = year + '_' + month + '_' +day;
            doc.save('GRN_' + $scope.GRN.GRN_CODE + '.pdf');
        }
    })