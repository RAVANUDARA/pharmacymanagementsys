'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const Suppliers = require('./public/js/Supplier');
const Users = require('./public/js/User');
const Item = require('./public/js/Item');
const PrescriptionModel = require('./public/js/Prescription');
const Category = require('./public/js/Category');
const purchaseOrder = require('./public/js/PurchaseOrder');
const GRN = require('./public/js/GRN');
const StockBalance = require('./public/js/StockBalance')
const DispenseModel = require('./public/js/Dispense');

var app = express();
app.use('/public', express.static(__dirname + '/public'))
app.use('/bower_components', express.static(__dirname + '/bower_components'))
app.use(bodyParser.json());

//Manage Login Part
app.get('/', function(req, res){
    res.sendFile(__dirname + '/public/login.html');
})

app.get('/index', function(req, res){
    res.sendFile(__dirname + '/public/index.html');
})

app.post('/login', function(req, res){
    console.log(req.body.name + ' ' + req.body.pass)
    Users.find({name: req.body.name, pass : req.body.pass}, function(err, User) {
        if(err)
            console.error('error of login');
        res.json(User);
    })
})
//End

//User Manamagement
app.get('/users', function(req, res){
    Users.find({}, function(err, UserList) {
        if(err)
            console.error('User cannot insert!');
        res.json(UserList);
    })
})

app.post('/users', function(req, res){
    var UserMaster = new Users(req.body);
    UserMaster.save( function (err, UserDetail){
        if(err)
            console.error('User cannot insert!');
        res.json(UserDetail);
    })
})

app.put('/users/:id', function (req, res) {
    Users.findById(req.params.id, function (err, User) {
        if(err)
            console.error('User data not fetch!');

        User.name = req.body.name || User.name;
        User.pass = req.body.pass || User.pass;
        User.type = req.body.type || User.type;
        User.editUser = req.body.editUser || User.editUser;
        User.editUser = req.body.editUser || User.editUser;

        User.save(function (err, UserDetails) {
            if(err)
                console.error('User data not fetch!');
            res.json(UserDetails);
        });
    });
});

app.delete('/users/:id', function (req, res) {
    Users.findById(req.params.id, function (err, User) {

        if(err || (User == null)) {
            console.error("User cannot find");
            res.json({"message": "User not find deleted"});
        }else {
            User.remove();
            res.json({"message" : "User was deleted", "data" : User});
        }
    })
});

//END

//Sachintha Part
//Supplier Management
//Start
app.get('/suppliers', function(req, res){
    Suppliers.find({}, function(err, SupplierList) {
        if(err)
            console.error('Suppliers cannot fetch!');
        res.json(SupplierList);
    })
})

app.post('/suppliers', function(req, res){
    var SupplierMaster = new Suppliers(req.body);
    SupplierMaster.save( function (err, Supplier){
        if(err)
            console.error('Supplier cannot insert!');
        res.json(Supplier);
    })
})

app.put('/suppliers/:id', function (req, res) {
    Suppliers.findById(req.params.id, function (err, Supplier) {
        if(err)
            console.error('Supplier data not fetch!');

        Supplier.name = req.body.name || Supplier.name;
        Supplier.add = req.body.add || Supplier.add;
        Supplier.mob = req.body.mob || Supplier.mob;
        Supplier.land = req.body.land || Supplier.land;
        Supplier.fax = req.body.fax || Supplier.fax;
        Supplier.catogary = req.body.catogary || Supplier.catogary;
        Supplier.regNo = req.body.regNo || Supplier.regNo;
        Supplier.editUser = req.body.editUser || Supplier.editUser;
        Supplier.editUser = Date.now() || Supplier.editUser;

        Supplier.save(function (err, SupplierDetails) {
            if(err)
                console.error('Supplier data not fetch!');
            res.json(SupplierDetails);
        });
    });
});

app.delete('/suppliers/:id', function (req, res) {
    Suppliers.findById(req.params.id, function (err, Supplier) {

        if(err || (Supplier == null)) {
            console.error("Supplier cannot find");
            res.json({"message": "Supplier not find deleted"});
        }else {
            Supplier.remove();
            res.json({"message" : "Supplier was deleted", "data" : Supplier});
        }
    })
});


app.get('/GRN', function(req, res){
    GRN.find({}, function(err, GRNList) {
        if(err)
            console.error('Suppliers cannot fetch!');
        res.json(GRNList);
    })
})

app.get('/GRN/:id', function(req, res){
    GRN.find({GRN_CODE : req.params.id}, function(err, GRNList) {
        if(err)
            console.error('Suppliers cannot fetch!');
        res.json(GRNList);
    })
})

app.post('/GRN', function(req, res){
    var GRNMaster = new GRN(req.body);
    GRNMaster.save( function (err, grn){
        if(err)
            console.error('Supplier cannot insert!');
        res.json(grn);
    })
})

app.put('/GRN/:id', function (req, res) {
    GRN.findById(req.params.id, function (err, grn) {
        if(err)
            console.error('Supplier data not fetch!');

        grn.GRN_CODE = req.body.GRN_CODE || grn.GRN_CODE;
        grn.PDate = req.body.PDate || grn.PDate;
        grn.items = req.body.items || grn.items;
        grn.user = req.body.user || grn.user;

        grn.save(function (err, GRNDetails) {
            if(err)
                console.error('GRN data not fetch!');
            res.json(GRNDetails);
        });
    });
});

app.delete('/GRN/:id', function (req, res) {
    GRN.findById(req.params.id, function (err, grn) {

        if(err || (grn == null)) {
            console.error("GRN cannot find");
            res.json({"message": "GRN not find deleted"});
        }else {
            grn.remove();
            res.json({"message" : "GRN was deleted", "data" : grn});
        }
    })
});

app.post('/stockBalance', function(req, res){
    StockBalance.find({ItemName : req.body.ItemName}, function(err, List) {
        if(err)
            console.error('Suppliers cannot fetch!');

            req.body.batchId = List.length + 1;
            var StockBalanceMaster = new StockBalance(req.body);
            StockBalanceMaster.save( function (err, StockBalanceList){
                if(err)
                    console.error('Stock balance cannot insert!');
                res.json(StockBalanceList);
            })
    })
})
//Sachintha Part
//END

//Yasitha Part
//Supplier Management
//Start

//Item Data Manupulate

app.get('/ItemMasterDetails',function(req,res,next) {
    Item.find({}, function (err, ItemMaster){
        res.json(ItemMaster);
        if (err) {
            console.log('<h1>Error Occured</h1>')
        }
    })
})

app.delete('/ItemMasterDetails/:id',function(req,res,next) {
    Item.findByIdAndRemove(req.params.id, function (err, ItemMaster)  {
        res.json(ItemMaster);
        if (err) {
            console.log('<h1>Error Occured</h1>')
        }
    })

})

app.post('/ItemMasterDetails',function(req,res){
    //res.send('got a post request');
    var ItemMaster = new Item(req.body);
    ItemMaster.save(function (err,ItemMaster){
        res.json(ItemMaster)
})

})

app.get('/ItemMasterDetails/:str',function(req,res,next) {
    Item.find({ ItemName:{ $regex: req.params.str, $options: "i" }}, function(err, result) {
        res.json(result);
    if (err) {
        console.log('<h1>Error Occured</h1>')
    }
})
})




app.get('/ItemMasterDetailsBycatogory/:str',function(req,res,next) {
    Item.find({ product_id:{ $regex: req.params.str, $options: "i" }}, function(err, result) {
        res.json(result);
    if (err) {
        console.log('<h1>Error Occured</h1>')
    }
})
})

//update category details
app.put('/ItemMasterDetails/',function(req,res,next) {
    var item = new Item(req.body);

    item.update({ _id : item._id,}, {
        $set:{
            product_id	  : item.product_id,
            ItemName      : item.ItemName,
            Description   :  item.Description ,
            Type          :  item.Type,
            ReOrderLevel  :  item.ReOrderLevel,
            SellPrice     :  item.SellPrice,
            User          : item.User
        }}, function (err, result)  {
        res.json(result);
        if (err) {
            console.log('<h1>Error Occured</h1>')
        }
    })

})

// Category Data Manupulate

app.post('/CategoryDetails',function(req,res){
    var cat = new Category(req.body);
    console.log(cat)
    cat.save(function(err,result){
        res.json(result)
    })

})

app.get('/CategoryDetails',function(req,res,next) {
    Category.find({}, function(err, result)  {
        res.json(result);
        if (err) {
            console.log('<h1>Error Occured</h1>')
        }
    })
})


app.get('/CategoryDetails/:str',function (req,res,next) {
    Category.find({ CategoryName:{ $regex: req.params.str, $options: "i" }}, function(err, result)  {
        res.json(result);
        if (err) {
            console.log('<h1>Error Occured</h1>')
        }
    })
})

app.delete('/CategoryDetails/:id',function(req,res,next) {
    Category.findByIdAndRemove(req.params.id, function(err, result)  {
        res.json(result);
        if (err) {
            console.log('<h1>Error Occured</h1>')
        }
    })

})

//update category details
app.put('/CategoryDetails/',function(req,res,next) {
    var cat = new Category(req.body);

    cat.update({ _id : cat._id,}, {
        $set:{CategoryName :cat.CategoryName,
            Description  : cat.Description,
            categoryType:cat.categoryType}}, function(err, result){
        res.json(result);
    if (err) {
        console.log('<h1>Error Occured</h1>')
    }
})

})

// Category Data Manupulate


app.post('/puchaseOrder',function(req,res){

    var pur = new purchaseOrder(req.body);
    console.log[pur]
    console.log(pur)
    pur.save(function(err,result){
        res.json(result)
    })
})

//to get all purchase data
app.get('/puchaseOrder',function(req,res,next) {
    purchaseOrder.find({}, function(err, result)  {
        res.json(result);
        if (err) {
            console.log('<h1>Error Occured</h1>')
        }
    })
})

app.get('/puchaseOrder/:id',function(req,res,next) {
    purchaseOrder.findById(req.params.id, function(err, result)  {
        res.json(result);
        if (err) {
            console.log('<h1>Error Occured</h1>')
        }
    })
})

// to delete purchase
app.delete('/puchaseOrder/:id',function(req,res,next) {
    purchaseOrder.findByIdAndRemove(req.params.id, function(err, result)  {
        res.json(result);
        if (err) {
            console.log('<h1>Error Occured</h1>')
        }
    })

})


app.post('/puchaseOrderDetails',function(req,res){

    var pur = new purchaseOrder(req.body);
    console.log(pur)
    pur.save(function(err,result){
        res.json(result)
    })
})
//Yasitha
//END

//Tharidu
//Start
app.get('/prescriptions',function (req,res) {
    PrescriptionModel.find().then(function (prescriptions) {
        res.json(prescriptions);
    }).catch(function (err) {
        console.error(err);
        res.sendStatus(500);
    });
});

app.get('/prescriptions/:id',function (req,res) {
    PrescriptionModel.findById(req.params.id).then(function (prescription) {
        res.json(prescription);
    }).catch(function (err) {
        console.error(err);
        res.sendStatus(500);
    });
});

app.get('/patients/:patientId',function (req,res) {
    PrescriptionModel.find({'patientId':req.params.patientId}).then(function (prescription) {
        res.json(prescription);
    }).catch(function (err) {
        console.error(err);
        res.sendStatus(500);
    });
});

app.post('/prescriptions',function (req,res) {
    const tempPrescription  = new PrescriptionModel(req.body);
    tempPrescription.save().then(function (prescription) {
        res.json(prescription);
    }).catch(function (err) {
        console.error(err);
        res.sendStatus(500);
    });
});

app.put('/prescriptions/:id',function (req,res) {
    const prescription = req.body;
    delete prescription._id;
    PrescriptionModel.findByIdAndUpdate(req.params.id,{$set:prescription}).then(function (prescription) {
        res.json(prescription);
    }).catch(function (err) {
        console.error(err);
        res.sendStatus(500);
    })
});

app.delete('/prescriptions/:id',function (req,res) {
    PrescriptionModel.findByIdAndRemove(req.params.id).then(function () {
        res.sendStatus(200);
    }).catch(function (err) {
        console.error(err);
        res.sendStatus(500);
    });
});

app.get('/drugs',function (req,res) {
    Item.find().then(function (drugs) {
        res.json(drugs);
    }).catch(function (err) {
        console.error(err);
        res.sendStatus(500);
    });
});
//Tharindu
//END

//Arabi
//Start
app.get('/dispense',function(req,res){
    DispenseModel.find({}, function(err, dispense) {
        if(err)
            console.error('User cannot insert!');
        res.json(dispense);
    })
});

app.post('/dispense',function(req,res) {
    console.log(req.body);
    const dispenseNew = new DispenseModel(req.body);
    dispenseNew.save().then(function(err, dispense){
        res.json(dispense);
    })
});


app.put('/dispense',function(req,res){
    const id = req.params.id;
    DispenseModel.findOneAndUpdate(req.body).exec().then(dispense)
    {
        res.json(dispense);
    }
});


app.delete('/dispense/:id', function(req, res) {
    DriverModel.findByIdAndRemove(req.params.id).then(function(dispense)  {
        const paymentId = dispense.Dispenses.map(paymentId);
        return DispenseModel.remove({_id: {$in: paymentId}});
    }).then(function () {


        res.sendStatus(200);
    }).catch (function(err){
        console.error(err);
        res.sendStatus(500);
    });
});

//Arabi
//END

app.listen(3000, function ( err) {
    if(err){
        console.log(err);
    }
    else
        console.log('Server successfully connected for Port No. 3000');
})
